

import csv
import sys
from pytz import timezone
from datetime import datetime

# print "This is the name of the script: ", sys.argv[0]
fName=sys.argv[1]
tz =  timezone('Europe/Prague')
# print "Time zone %s" % tz

with open(fName, mode='r') as infile:
    reader =  csv.DictReader(infile)
    for ln in reader:
        d = ln.get('dat')
        t = ln.get('cas')
        dt = "%s %s" % (d, t)
        dt_obj = tz.localize(datetime.strptime(dt, '%d.%m.%Y %H:%M'))
 
        #print(dt_obj) 
        del ln['dat']
        del ln['cas']
        #print(ln)
 
	for kln in ln:
            print("meteo.hvezdarna.%s %s %s" % (kln, ln[kln], dt_obj.strftime("%s")))





