# Data description
```
;##=Name                     ,Unit ,DBF       ,T, N,n,Dev,Mod,Input ,DigMin,DigNul,DigMax, *AAA  , +BBB  , Max
001=Vlhkost vzduchu          ,%    ,VlVzd     ,N, 6,1,  1,   ,000+2 ,     0,     0, 32750, 0.010 , 0     , 100
011=Teplota                  ,°C   ,Tep2m     ,N, 6,1,  1,   ,002+2 ,-32750,-32750, 32750, 0.010 , 0     , 
012=Teplota min.             ,°C   ,Tep2m_I   ,N, 6,1,  1,   ,004+2 ,-32750,-32750, 32750, 0.010 , 0     , 
013=Teplota max.             ,°C   ,Tep2m_X   ,N, 6,1,  1,   ,006+2 ,-32750,-32750, 32750, 0.010 , 0     , 
016=Rosný bod                ,°C   ,RosBod    ,N, 6,1,  1,   ,CALCUL, DewPoint(meteo.Tep2m,meteo.VlVzd)
021=Tlak absolutní           ,hPa  ,Tlak      ,N, 6,1,  1,   ,008+2 ,-32750,-32750, 32750, 0.100 , 0     , 
022=Tlak přepočtený na moře  ,hPa  ,Tlak_M    ,N, 6,1,  1,   ,010+2 ,-32750,-32750, 32750, 0.100 , 0     , 
031=Osvětlení                ,Lx   ,Osvit     ,N, 6,0,  1,   ,012+2 ,-32750,-32750, 32750,10.000 , 0     , 
032=Radiace                  ,W/m2 ,Radiace   ,N, 6,0,  1,   ,014+2 ,-32750,-32750, 32750, 1.000 , 0     ,
033=Radiace průměrná         ,W/m2 ,Radiac_A  ,N, 6,0,  1,   ,016+2 ,-32750,-32750, 32750, 1.000 , 0     ,
041=Rychlost větru           ,m/s  ,RychV     ,N, 6,1,  1,   ,018-2 ,     0,     0, 32750, 0.010 , 0     ,
042=Směr     větru           ,°    ,SmerV     ,N, 6,0,  1,   ,020-2 ,     0,     0,   360, 1.000 , 0     ,
043=Rychlost větru průměr    ,m/s  ,RychV_P   ,N, 6,1,  1,   ,022-2 ,     0,     0, 32750, 0.010 , 0     ,
044=Směr     větru průměr    ,°    ,SmerV_P   ,N, 6,0,  1,   ,024-2 ,     0,     0,   360, 1.000 , 0     ,
045=Rychlost větru maxim     ,m/s  ,RychV_X   ,N, 6,1,  1,   ,026-2 ,     0,     0, 32750, 0.010 , 0     ,
046=Směr     větru maxim     ,°    ,SmerV_X   ,N, 6,0,  1,   ,028-2 ,     0,     0,   360, 1.000 , 0     ,
047=Čas maxima větru         ,s    ,CasV_X    ,N, 6,0,  1,   ,030-2 ,     0,     0,   600, 1.000 , 0     ,
048=Dráha větru              ,m    ,DrahaV    ,N, 6,0,  1,   ,032-2 ,     0,     0, 65500, 1.000 , 0     ,
050=Srážky                   ,mm   ,Srazky    ,N, 6,1,  1,   ,034+2 ,     0,     0, 32750, 0.100 , 0     , 
051=Srážky  1. minuta        ,mm   ,Sra01     ,N, 6,1,  1,   ,036-1 ,     0,     0,   250, 0.1   , 0     , 
052=Srážky  2. minuta        ,mm   ,Sra02     ,N, 6,1,  1,   ,037-1 ,     0,     0,   250, 0.1   , 0     , 
053=Srážky  3. minuta        ,mm   ,Sra03     ,N, 6,1,  1,   ,038-1 ,     0,     0,   250, 0.1   , 0     , 
054=Srážky  4. minuta        ,mm   ,Sra04     ,N, 6,1,  1,   ,039-1 ,     0,     0,   250, 0.1   , 0     , 
055=Srážky  5. minuta        ,mm   ,Sra05     ,N, 6,1,  1,   ,040-1 ,     0,     0,   250, 0.1   , 0     , 
056=Srážky  6. minuta        ,mm   ,Sra06     ,N, 6,1,  1,   ,041-1 ,     0,     0,   250, 0.1   , 0     , 
057=Srážky  7. minuta        ,mm   ,Sra07     ,N, 6,1,  1,   ,042-1 ,     0,     0,   250, 0.1   , 0     , 
058=Srážky  8. minuta        ,mm   ,Sra08     ,N, 6,1,  1,   ,043-1 ,     0,     0,   250, 0.1   , 0     , 
059=Srážky  9. minuta        ,mm   ,Sra09     ,N, 6,1,  1,   ,044-1 ,     0,     0,   250, 0.1   , 0     , 
060=Srážky 10. minuta        ,mm   ,Sra10     ,N, 6,1,  1,   ,045-1 ,     0,     0,   250, 0.1   , 0     , 
071=Detekce deště            ,s    ,Dest      ,N, 6,0,  1,   ,046+2 ,     0,     0, 32750, 1.000 , 0     ,
098=Napětí baterie minimum   ,V    ,Nabat_I   ,N, 6,1,  1,   ,048+2 ,     0,     0, 32750, 0.010 , 0     , 
099=Napětí baterie           ,V    ,Nabat     ,N, 6,1,  1,   ,050+2 ,     0,     0, 32750, 0.010 , 0     , 
[limits]
;#=Value     , MinR, MaxR,Sound
01=Nabat    , 12.5, 15.0, 1  
[immediate]
;#=Type  , Top,Left,Size,Value1    ,Value2
01=Item  ,  30,  10,    ,VlVzd     
02=Item  ,  50,  10,    ,Tep2m     
16=Item  ,  70,  10,    ,RosBod     
21=Item  ,  90,  10,    ,Tlak_M    
31=Item  , 110,  10,    ,Osvit    
32=Item  , 130,  10,    ,Radiace    
40=Vane  ,  10, 290,    ,SmerV   ,RychV 
41=Item  , 190, 260,    ,RychV   
42=Item  , 210, 260,    ,SmerV  
51=Item  , 160,  10,    ,Srazky    
71=Item  , 180,  10,    ,Dest   
99=Item  , 210,  10,    ,Nabat   
[graph]
;#=Value     ,   Min,   Max, Step,Shift,Style
01=Tep2m
02=Osvit     ,     0, 60000,10000
03=Tlak_M    ,   960,  1060,   20
04=VlVzd
05=Radiace
06=RychV
07=SmerV
08=Srazky
```
